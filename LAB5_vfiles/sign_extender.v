`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    13:40:54 10/20/2011
// Design Name:
// Module Name:    SignExtend
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module sign_extender(
       in16,
       out32
   );

       input [15:0] in16;
       output [31:0] out32;
// Or them!
or or_1[15:0] (out32[15:0],in16,1'b0);
or or_2[15:0] (out32[31:16],in16[15],1'b0);


endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:52:04 10/20/2011 
// Design Name: 
// Module Name:    control 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module control( InstrIn, ALUOUT, reset, clk , write );

parameter SIZE = 32;

input write;
input [SIZE-1:0] InstrIn;
input reset, clk;
output wire [SIZE-1:0] ALUOUT;

wire [SIZE-1:0] readData1,readData2, ALU1, ALU2, muxRD2, Imm, ALU_RESULT;
wire [25:0] s2_out;
wire [SIZE-1:0] s1_out;
wire [5:0] s3_out;
wire arithmetic ;


// STAGE 1
nbit_reg #(SIZE) stage1(InstrIn , s1_out , write, reset ,clk );

// REG FILE
nbit_register_file #(5,32) regfile(ALUOUT,readData1,readData2,s1_out[20:16],s1_out[15:11],s3_out[4:0],s3_out[5],reset,clk);

// STAGE 2
// 16 bit - immediate
// 1 bit for write enable, 1 bit for DataSRC,  3 bits for ALU OP - 5 bits for Write select  = 26 bits
nbit_reg #(26) stage2_instruction({s1_out[15:0],s1_out[30:21]},s2_out,write, reset, clk);
nbit_reg #(SIZE) stage2_read1(readData1,ALU1,write,reset,clk);
nbit_reg #(SIZE) stage2_read2(readData2,muxRD2,write,reset,clk);

// Out into Sign Extends
sign_extender se3(s2_out[25:10],Imm);

// MUX the DATA Source
nbitMUX #(SIZE) ALUSRC(muxRD2,Imm,s2_out[8],ALU2);

// INTO ALU
alu #(SIZE) myALU(ALU1,ALU2,reset,s2_out[7:5],ALU_RESULT);

// OUT OF ALU into Write
nbit_reg #(6) stage3_instruction({s2_out[9],s2_out[4:0]},s3_out,write,reset,clk);
nbit_reg #(32) stage3_result(ALU_RESULT,ALUOUT,write,reset,clk);

/*
module nbit_register_file(WriteData,   // Input data
                          ReadData1,   // Output data 1
                          ReadData2,   // Output data 2
                          ReadSelect1, // Select lines for output 1
                          ReadSelect2, // Select lines for output 2
                          WriteSelect, // Select lines for input
                          WriteEnable, // Causes write to take place on posedge
                          Reset,       // Synchronous reset
                          Clk);        // Clock
*/



// Put R1 in Write Select
//sl_out[25:21];

//Put R2 in Read 1 
//s1_out[20:16];

// Put Imm in Immediate not in stage 1
//s1_out[15:0];

// Put R3 in Read 2
//s1_out[15:11];

// If true most
//s1_out[29];






endmodule



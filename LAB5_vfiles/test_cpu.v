`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:14:13 10/20/2011
// Design Name:   control
// Module Name:   /home/samahmed/Desktop/LAB5_vfiles/test_cpu.v
// Project Name:  samirAhmed_lab5
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: control
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_cpu;

	// Inputs
	reg [31:0] InstrIn;
	reg reset;
	reg clk;
	reg write;

	// Outputs
	wire [31:0] ALUOUT;

	// Instantiate the Unit Under Test (UUT)
	control uut (
		.InstrIn(InstrIn), 
		.ALUOUT(ALUOUT), 
		.reset(reset), 
		.clk(clk),
		.write(write)
	);

	initial begin
		// Initialize Inputs
		write = 1'b1;
		InstrIn = 0;
		reset = 1;
		clk = 0;
		
		
		//------------TEST ADD --------------------------------
		//
		// ADD R5 R0 !67 0xFF43 - I TYPE NOT : R6 = 67
		#20 InstrIn = 32'b0_1_1_010_00101_00000_00000_000_0100_0011; reset= 0;
		
		//------------TEST ADD AGAIN FOR FUN -------------------
		//
		// ADD R3 R3 10 - I TYPE ADD : NOW R3 = 10 
		#20 InstrIn = 32'b0_1_1_010_00011_00011_00000_000_0000_1010;
		
		//------------TEST ADD NEGATIVE --------------------------------
		//
		// ADD R2 R2 -1 - I TYPE ADD : NOW R2 = -1 
		#20 InstrIn = 32'b0_1_1_010_00010_00010_11111_111_1111_1111;
		
		// WAIT FOR 2 TICKS SO THAT ALL THE REGISTERS ARE UP TO DATE
		#40;
		
		//------------TEST SUBTRACT-----------------------------
		// 
		// SUB R4,R5,R3 - R type Subtract - R4 = R5 - R3 = 57   
		#20 InstrIn = 32'b0_1_0_011_00100_00101_00011_000_0000_0000;
		
		// THIS IS PROOF THAT WE HAVE READ FROM A REGISTER FILE THAT WE WROTE TO
		
		// -----------TEST MOV -------------------------------
		// 
		// MO R1 R5  - R TYPE MOV : EXPECT  R1 = 67
 		#20 InstrIn = 32'b0_1_0_000_00001_00101_00000_000_0000_0000;
		
		// -----------TEST NOT --------------------------------
		//
		// NOT R7 R5 - R TYPE NOT : EXPECT R7 = -68
		#20 InstrIn = 32'b0_1_0_001_00111_00101_00000_000_0000_0000;
		
		//------------TEST SUB - I TYPE -----------------------
		//
		// NOT R8 R5 -256 - I TYPE SUBTRACT - EXPECT R8 = 67 - -256 = 323
		#20 InstrIn = 32'b0_1_1_011_01000_00101_11111_111_0000_0000;
		
		//------------TEST OR - I TYPE ------------------------
		//
		// OR R9 R5 111100b  - GIVES R9 = 127
		#20 InstrIn = 32'b0_1_1_100_01001_00101_00000_000_0011_1100;
		
		#40; /// WAIT FOR 2 MORE TICKS ,, JUST TO BREAK IT UP
		
		//------------TEST OR - R TYPE -------------------------
		//
		// OR R10 R10 R5 - GIVES SAME THING there for R10 = 67
		#20 InstrIn = 32'b0_1_0_100_01010_01010_00101_000_0000_0000;
		
		//------------TEST AND - R TYPE ----------------------
		//
		// AND R11 R2 R5 - THEREFORE R11 = 1111... AND ANYTHING should give SAME THING i.e R11 = 67 
		#20 InstrIn = 32'b0_1_0_101_01011_00010_00101_000_0000_0000;
		
		//------------ TEST AND - I TYPE ----------------------
		//
		// AND R12 R4 -2 - should give CLEAR FIRST BIT -> R12 = 56   
		#20 InstrIn = 32'b0_1_1_101_01100_00100_11111_111_1111_1110;
		
		//------------ TEST XOR - R TYPE
		//
		// XOR R13 R13 R5 - R14 = 67
		#20 InstrIn = 32'b0_1_0_110_01101_01101_00101_000_0000_0000;
		
		//------------ TEST XOR I TYPE
		//
		// XOR R14 R5 1  -> R15 67-1 = 66 
		#20 InstrIn = 32'b0_1_1_110_01110_00101_00000_000_0000_0001;	
		
		//------------ TEST SLT R TYPE
		//
		// SLT R15 R5 R5 -> R5 = 67 is not less than 67 -> RESULT = 0
		#20 InstrIn = 32'b0_1_0_111_01111_00101_00101_000_0000_0000;
		
		//------------ TEST SLT I TYPE
		//
		// SLT R16 R7 -1  -> Check if R7 (-68) < -1 Should give us a 1!
		#20 InstrIn = 32'b0_1_1_111_10000_00111_11111_111_1111_1111;
		

		
		#100 $finish;
	end
			always begin 
			# 10 clk = ~clk;
			end			

      
endmodule


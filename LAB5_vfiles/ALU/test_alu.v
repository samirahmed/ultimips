`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:29:02 10/04/2011
// Design Name:   alu
// Module Name:   /ad/eng/users/c/r/crowell/Desktop/lab4_samir/lab4/test_alu.v
// Project Name:  lab4
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: alu
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

`define SIZE 4
module test_alu;

	//parameter SIZE=32;
	// Inputs
	reg [`SIZE-1:0] R2;
	reg [`SIZE-1:0] R3;
	reg cin;
	reg [2:0] select;

	// Outputs
	wire [`SIZE-1:0] Rout;

	// Instantiate the Unit Under Test (UUT)
	alu #(`SIZE) uut (
		.R1(R2), 
		.R2(R3), 
		.cin(cin), 
		.select(select), 
		.Rout(Rout)
	);

	initial begin
	
		
// Initialize Inputs
		cin = `SIZE'd0;
		select = 7;

		R2 = 1; R3 = -1;
		#100; 
		R2 = -8; R3 = 5;
		#100; R2 = -4; R3 = -3;
		R2 = 3; R3 = 6;
		#100; 
		R2 = 7; R3 = 1;

		#100; 
		R2 = 4; R3 = -7;
		#100; 
		R2 = 7; R3 = -8;
		#100; 
		R2 = -8; R3 = -8;
		#100; 
				
	end
      
endmodule


`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:24:03 09/27/2011 
// Design Name: 
// Module Name:    adder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define D 0
module adder (
         A,
			B, 
         cin,
			sum,
         cout
    );
    
   
    input   cin;
    input   A;
    input   B;
    output  sum;
    output  cout;
    wire aXb;
         wire cinXaxb;
         wire AND_1;
         wire AND_2;
         
         // a xor b
         xor_gate aXORb (A,B,aXb);
         
         // c xor aXb
         xor_gate cXORaxb (aXb,cin,sum);
         
         // cin and aXb
         and #`D cinANDaxb (AND_1,aXb,cin);
                 
         // a and b
         and #`D aANDb (AND_2,A,B);
         
         // AND_2 or AND_1 
         or #`D OR_1(cout,AND_2,AND_1);
         
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:48:32 09/27/2011 
// Design Name: 
// Module Name:    mux16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define D 0

module mux_3bit(select,out,
					 mod0,mod1,mod2,mod3,mod4,mod5,mod6,mod7);

parameter SIZE =32;

// Input select and output
input 	[2:0] select;
output 	[SIZE-1:0] out;

// All the inputs
input  	[SIZE-1:0] mod0;
input  	[SIZE-1:0] mod1;
input  	[SIZE-1:0] mod2;
input  	[SIZE-1:0] mod3;
input  	[SIZE-1:0] mod4;
input  	[SIZE-1:0] mod5;
input  	[SIZE-1:0] mod6;
input  	[SIZE-1:0] mod7;

// wires
wire 		[SIZE-1:0] out_m0;
wire 		[SIZE-1:0] out_m1;
wire 		[SIZE-1:0] out_m2;
wire 		[SIZE-1:0] out_m3;
wire 		[SIZE-1:0] out_m4;
wire 		[SIZE-1:0] out_m5;

// Mux Pyramid
//
// M0
//		M4
// M1		
//			M6
// M2
//		M5
// M3

// Tier 1
nbitMUX #(SIZE)	m1(mod0,mod1,select[0],out_m0);
nbitMUX #(SIZE)	m2(mod2,mod3,select[0],out_m1);
nbitMUX #(SIZE)	m3(mod4,mod5,select[0],out_m2);
nbitMUX #(SIZE)	m4(mod6,mod7,select[0],out_m3);

// Tier 2
nbitMUX #(SIZE)	m5(out_m0,out_m1,select[1],out_m4);
nbitMUX #(SIZE)	m6(out_m2,out_m3,select[1],out_m5);

// Tier 3
nbitMUX #(SIZE)	m7(out_m4,out_m5,select[2],out);

endmodule

module nbitMUX(A,B,S,Out);

parameter SIZE=32;

input [SIZE-1:0] A;
input [SIZE-1:0] B;
input	S;
output [SIZE-1:0] Out;

Mux21 muxArray[SIZE-1:0] (A[SIZE-1:0],B[SIZE-1:0],S,Out[SIZE-1:0]);

endmodule

module Mux21(A, B, S, Out);

			
        input wire A, B, S;
        output wire Out;
        wire notS, ATemp, BTemp;

        // Standard Mux
        not invertS (notS, S);
        and  andA (ATemp, A, notS);
        and  andB (BTemp, B, S);
        or  result(Out, ATemp, BTemp);

endmodule

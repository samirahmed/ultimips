
############################################################################################################################
### ALU MODULES ############################################################################################################

Sections
All the sections below are parameterized so that they can accept a variable bit size

MOVER - Take an input B and return the output which is the result OR-ed with zero. so it returns the result

NOTTER - Take an input B and return the output passed through an NOT gate array

ADDER - Take an input A,B and CIN, then create a parameterized array of ADDERS (made from lab 3 adders) in which we chain the carry outs to the carry ins ...

SUBTRACTOR - Take inputs A,B and then flip the B, then use the parameterized adder with CIN=1  to add !B with A to give A-B

ANDER - Take an input A,B, use a parameterized array of primitive AND gates to AND A with B

ORER - Take an input A,B use a parameterized array of primitive OR gates to OR A with B

XORER - Take an input A,B use a parameterized array of XOR_GATES (Taken from lab 3 XOR_GATE)

SHIFT_LESS_THAN - Take an input A,B - We subtract using the SUBTRACTOR and then take the most significant bit and return it

###########################################################################################################################


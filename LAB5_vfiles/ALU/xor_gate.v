`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:11:47 09/27/2011 
// Design Name: 
// Module Name:    xor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define D 0
module xor_gate( A,B,out );

    input   A;
    input   B;
    output  out;
	 
	 // Not B
	 wire notb;
	 not #`D NOT_B (notb,B);
	 
	 // A.NOTB
	 wire anotb;
	 and #`D A_AND_NOTB (anotb,A,notb);
	 
	 // Not B
	 wire nota;
	 not #`D NOT_A (nota,A);
	 
	 // A.NOTB
	 wire bnota;
	 and #`D B_AND_NOTA (bnota,B,nota);
	 
	 // A not B  or  B not A
	 or #`D OR1 (out,anotb,bnota);
	 
endmodule

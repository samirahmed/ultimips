`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:37:58 10/04/2011 
// Design Name: 
// Module Name:    alu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define D 0 
//---- ALU TOP MODULE --------------------------------

module alu (R1,R2,cin,select,Rout);

parameter SIZE =32;

input [SIZE-1:0] R1;
input [SIZE-1:0] R2;
input cin;
input [2:0] select;
output [SIZE-1:0] Rout;

// my wires for the module outputs
wire [SIZE-1:0] w0;
wire [SIZE-1:0] w1;
wire [SIZE-1:0] w2;
wire [SIZE-1:0] w3;
wire [SIZE-1:0] w4;
wire [SIZE-1:0] w5;
wire [SIZE-1:0] w6;
wire [SIZE-1:0] w7;

// All my modules
mover #(SIZE) sel0(w0,R1);
notter #(SIZE) sel1(w1,R1);
nbitadder #(SIZE) sel2(R1,R2,cin,w2);
subtractor #(SIZE) sel3(R1,R2,w3);
orer #(SIZE) sel4(R1,R2,w4);
ander #(SIZE) sel5(R1,R2,w5);
xorer #(SIZE) sel6(R1,R2,w6);
slt #(SIZE) sel7(R1,R2,w7);

// Put them in the mux

mux_3bit #(SIZE) muxes(select,Rout,w0,w1,w2,w3,w4,w5,w6,w7);

endmodule


//---- N AND OR MODULE -------------------------------

module ander(A,B, out);

parameter SIZE =32;

input [SIZE-1:0] A;
input [SIZE-1:0] B;
output [SIZE-1:0] out;

and  nbitAND[SIZE-1:0] (out[SIZE-1:0],A[SIZE-1:0],B[SIZE-1:0]);

endmodule

//---- N BIT OR MODULE -------------------------------

module orer(A,B,out);

parameter SIZE =32;

input [SIZE-1:0] A;
input [SIZE-1:0] B;
output [SIZE-1:0] out;

or  nbitOR[SIZE-1:0] (out[SIZE-1:0],A[SIZE-1:0],B[SIZE-1:0]);

endmodule

//---- N BIT XOR MODULE -------------------------------

module xorer(A,B,out);

parameter SIZE =32;

input [SIZE-1:0] A;
input [SIZE-1:0] B;
output [SIZE-1:0] out;

xor_gate nbitXOR[SIZE-1:0] (A[SIZE-1:0],B[SIZE-1:0],out[SIZE-1:0]);

endmodule

//----- N BIT MOVER -----------------------------------

module mover(R1,R2);

parameter SIZE =32;

input [SIZE-1:0] R2;
output [SIZE-1:0] R1;

or  nbitOR[SIZE-1:0] (R1[SIZE-1:0],R2[SIZE-1:0],1'b0);

endmodule

//----- N BIT NOTTER -----------------------------------

module notter(A,B);

parameter SIZE = 32;

input [SIZE-1:0] B;
output [SIZE-1:0] A;

not  nbitNOT[SIZE-1:0] (A[SIZE-1:0],B[SIZE-1:0]);

endmodule


//----- N BIT ADDER ------------------------------------

module nbitadder(A,B,cin,Sum);

parameter SIZE = 32;

input [SIZE-1:0] A;
input [SIZE-1:0] B;
input cin;
wire  [SIZE-1:0] cinwire;
output [SIZE-1:0] Sum;
wire cout;

// Combo Adder
adder bitAdder[SIZE-1:0] (A,B,{cinwire[SIZE-1:1],cin},Sum,{cout,cinwire[SIZE-1:1]});

endmodule

//----- N BIT SUBTRACTER -------------------------------

module subtractor(A,B,Sum);

parameter SIZE = 32;

wire  cin = 1;
input [SIZE-1:0] A;
input [SIZE-1:0] B;
wire  [SIZE-1:0] notB;
wire  [SIZE-1:0] cinwire;
output [SIZE-1:0] Sum;

// Use the notter to not all the B bits first
notter #(SIZE) not1(notB,B);

// Same thing as before except with out the full adder
nbitadder #(SIZE)  add1(A,notB,cin,Sum);

endmodule


module slt(A,B,out);

parameter SIZE=32;
input [SIZE-1:0] A;
input [SIZE-1:0] B;
wire  [SIZE-1:0] D ;
output [SIZE-1:0] out;
wire  nA,nB,nD;
wire term1, term2, term3, term4, all4;

// Get the difference of A-B and put it in D
subtractor #(SIZE) sub1(A,B,D);

// A' B' D  + A B' D' + A B' D + A B D

// Now create the nots
not notA (nA,A[SIZE-1]);
not notB (nB,B[SIZE-1]);
not notD (nD,D[SIZE-1]);

// Now create the four and terms
and notAnotBD (term1,nA,nB,D[SIZE-1]);
and AnotBnotD (term2,A[SIZE-1],nB,nD);
and AnotBD	  (term3,A[SIZE-1],nB,D[SIZE-1]);
and ABD		  (term4,A[SIZE-1],B[SIZE-1],D[SIZE-1]);

// Now or all 4 terms
or all_4_terms (all4,term1,term2,term3,term4);

// Put the result in a set of zeros with last digit being the answer
and  clear[SIZE-1:1] (out[SIZE-1:1],1'b0,1'b0);
or  andresult (out[0],1'b0,all4);

endmodule

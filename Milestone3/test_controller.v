`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:02:23 11/08/2011
// Design Name:   controller
// Module Name:   /home/samahmed/Desktop/Project/Milestone3/test_controller.v
// Project Name:  control
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: controller
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_controller;

	// Inputs
	reg [5:0] op;
	reg clk;
	reg reset;

	// Outputs
	wire [1:0] ALUSRCA;
	wire [2:0] ALUSRCB;
	wire [1:0] ALUOP;
	wire PCWRITE;
	wire PCWRITECOND;
	wire [1:0] PCSOURCE;
	wire READR1;
	wire MEM2REG;
	wire IORD;
	wire MEMREAD;
	wire MEMWRITE;
	wire IRWRITE;
	wire REGWRITE;

	// Instantiate the Unit Under Test (UUT)
	controller uut (
		.op(op), 
		.ALUSRCA(ALUSRCA), 
		.ALUSRCB(ALUSRCB), 
		.ALUOP(ALUOP), 
		.PCWRITE(PCWRITE), 
		.PCWRITECOND(PCWRITECOND), 
		.PCSOURCE(PCSOURCE), 
		.READR1(READR1), 
		.MEM2REG(MEM2REG), 
		.IORD(IORD), 
		.MEMREAD(MEMREAD), 
		.MEMWRITE(MEMWRITE), 
		.IRWRITE(IRWRITE), 
		.REGWRITE(REGWRITE), 
		.clk(clk), 
		.reset(reset)
	);

	initial begin
		// Initialize Inputs
		op = 0;
		clk = 0;
		reset = 0;

		// Wait 100 ns for global reset to finish
		#105;
		
		// Test JUMP
		op = 6'b000001;
      #30; 
		
		// Test LI
		op = 6'b111001;
		#40;
		
		// Test LUi
		op = 6'b111010;
		#40;
		
		// Test LWI
		op = 6'b111011;
		#50;
		
		// Test SWI
		op = 6'b111100;
		#40;
	
		// Test BNEQ
		op = 6'b100001;
		#30;
		
		// Test BEQZ
		op = 6'b100000;
		#30;
		
		// Test NOOP
		op = 6'b000000;
		#100;
		
		// Little Break...
		//
		// Testing all R Types
	
		#40; op = 6'b010000;	
		#40; op = 6'b010001;
		#40; op = 6'b010010;
		#40; op = 6'b010011;
		#40; op = 6'b010100;
		#40; op = 6'b010101;
		#40; op = 6'b010110;
		#40; op = 6'b010111;
		#40; op = 6'b000000;
		#100;
		
		// Testing all I Type with States 0>1>2>3
		
		#40; op = 6'b110010;
		#40; op = 6'b110011;
		#40; op = 6'b110100;
		#40; op = 6'b110101;
		#40; op = 6'b110110;
		#40; op = 6'b110111;
		#40; op = 6'b000000;
		#100;
	
		// Finish
		#100; $finish;

	end
   
	always begin
	#5; clk = ~clk; 
	end
	
		
endmodule


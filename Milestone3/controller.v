`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BU
// Engineer: SAMIR
// 
// Create Date:    17:38:11 11/07/2011 
// Design Name: 
// Module Name:    controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module controller(
    input [5:0] op,
    output reg [1:0] ALUSRCA,
    output reg [2:0] ALUSRCB,
    output reg [1:0] ALUOP,
    output reg PCWRITE,
    output reg PCWRITECOND,
    output reg [1:0] PCSOURCE,
    output reg READR1,
    output reg MEM2REG,
    output reg IORD,
    output reg MEMREAD,
    output reg MEMWRITE,
    output reg IRWRITE,
	 output reg REGWRITE,
	 input wire clk,
	 input wire reset
    );
	 
// State Registers
reg [2:0] state;
reg [2:0] next_state;

// If reset is true then 
always @( posedge clk)  begin
	if (reset) state = 4'b000;
	else state = next_state;
end

// ------ 8 State Machine ----------------------------------------
// Instruction Fetch 	(State 3'b000)
// Instruction Decode	(State 3'b001)
// Execution 				(State 3'b010, 3'b100, 3'b101, 3'b110)
// Memory					(State 3'b111)
// Write Back				(State 3'b011)

always @( posedge clk) begin
	case (state)
		3'b000: 	case (op)
							6'b000000 : next_state <=3'b000;	// NOOP: Loop to Instruction Fetch
							default   : next_state <=3'b001; // Instruction Fetch -> Instruction Decode
					endcase
		3'b001: 	case (op[5:3]) 
							3'b000 : next_state <=3'b101;		// Jump Instruction
							3'b010 : next_state <=3'b010;		// R Type Instruction
							3'b110 : next_state <=3'b010;		// I Type Instruction (non L/S)
							3'b111 : next_state <=3'b110;		// LWI,SWI, LI or LUI Type
							3'b100 : next_state <=3'b100;		// Branch Type
							default: next_state <=3'b000;		// All Exceptions go to Instruction Fetch
					endcase
		3'b010:  next_state <= 3'b011;						// R or I type Execute State -> Write Back 
		3'b011:	next_state <= 3'b000;						// Write Back -> Instruction Fetch
		3'b100:	next_state <= 3'b000;						// Branch Execute -> Instruction Fetch
		3'b101:	next_state <= 3'b000;						// Jump Execute -> Instruction Fetch
		3'b110:	case (op[2:0]) 
							3'b001 : next_state <=3'b011;		// LI:  Go to Write Back
							3'b010 : next_state <=3'b011;		// LUI: Go to Write Back
							3'b011 : next_state <=3'b111;		// LWI: Go to Memory
							3'b100 : next_state <=3'b111;		// SWI: Go to Memory
							default:	next_state <=3'b000;		// All Exceptions go to Instruction Fetch
					endcase
		3'b111:	case (op[2:0])
							3'b011 : next_state <= 3'b011;	// Memory -> Write Back
							3'b100 : next_state <= 3'b000;	// Memory -> Instruction Fetch
							default: next_state <= 3'b000;	// *All Exceptions go back to Instruction Fetch 
					endcase
		default:	next_state <= 3'b000;						// *All Exceptions go back to Instruction Fetch
	endcase
end

// Control States
//
//	ALUSRCA		= ALU R2 
// ALUSRCB	 	= ALU R3
// ALUOP	  		= ALU Function 
// MEMWRITE		= MEMORY WRITE ENABLE 
// MEMREAD		= MEMORY READ ENABLE
// MEM2REG		= READ DATA FROM MEMORY DATA REGISTER, READ DATA FROM ALUOUT
//	IRWRITE		= INSTRUCTION REGISTER WRITE ENABLE
//	PCSOURCE		= PROGRAM COUNTER SOURCE
// PCWRITE		= PROGRAM COUNTER OVERWRITE ENABLE
// PCWRITECOND = CONDITIONAL PROGRAM COUNTER OVERWRITE ENABLE
// READR1		= READ R1 INTO REGISTER FILE READ SELECT A
// IORD			= INSTRUCTION OR DATA MEMORY ACCESS
// REGWRITE		= ENABLED REGISTER FILE WRITE BACK

always @( posedge clk) begin
	
	case(state)
		3'b000 : 
			begin
					ALUSRCA		<= 2'b00;		//ALUSRCA = PC
					ALUSRCB		<= 3'b100;		//ALUSRCB = SHIFTED IMMEDIATED
					ALUOP			<= 2'b00;		//ALUOP = ALWAYS ADD
					PCWRITE		<= 1'b1;			//PCWRITE = ENABLED
					PCWRITECOND	<= 1'b0;
					PCSOURCE		<= 2'b00;		//PCSOURCE = PC+4
					READR1		<= 1'b0;
					MEM2REG		<= 1'b0;
					IORD			<= 1'b0;			//IORD = TAKE INSTRUCTION ADDRESS
					MEMREAD		<=	1'b1;			//MEMREAD = ENABLED
					MEMWRITE		<= 1'b0;			
					IRWRITE 		<= 1'b1;			//IRWRITE = ENABLED
					REGWRITE		<= 1'b0;
			end
		3'b001 :
			begin
					ALUSRCA		<= 2'b00;		//ALUSRCA = PC
					ALUSRCB		<= 3'b010;		//ALUSRCB = SE Immediate
					ALUOP			<= 2'b00;		//ALUOP 	= ALWAYS ADD
					PCWRITE		<= 1'b0;
					PCWRITECOND	<= 1'b0;
					PCSOURCE		<= 2'b00;
					case (op)
						6'b111100 : READR1 <= 1'b1;	// SWI: 	READR1 = ENABLED
						6'b111010 : READR1 <= 1'b1;	// LUI:	READR1 = ENABLED
						6'b111001 : READR1 <= 1'b1;	// LI:	READR1 = ENABLED
						6'b100000 : READR1 <= 1'b1;	// BEQZ: READR1 = ENABLED
						6'b100001 : READR1 <= 1'b1;	// BNEZ: READR1 = ENABLED
						default   : READR1 <= 1'b0;	// ALL OTHER CASES: READR1 = DISABLED
					endcase					
					MEM2REG		<= 1'b0;
					IORD			<= 1'b0;
					MEMREAD		<=	1'b0;
					MEMWRITE		<= 1'b0;
					IRWRITE 		<= 1'b0;
					REGWRITE		<= 1'b0;
			end
		3'b010 :
			begin
					ALUSRCA		<= 2'b01;				// ALUSRCA	= A
					ALUSRCB		<= {2'b00,op[5]};		//	ALUSRCB	= Immediate or B
					ALUOP			<= 2'b10;				// ALUOP		= TAKE OPCODE FUNCTION
					PCWRITE		<= 1'b0;
					PCWRITECOND	<= 1'b0;
					PCSOURCE		<= 2'b00;
					READR1		<= 1'b0;
					MEM2REG		<= 1'b0;
					IORD			<= 1'b0;
					MEMREAD		<=	1'b0;
					MEMWRITE		<= 1'b0;
					IRWRITE 		<= 1'b0;
					REGWRITE		<= 1'b0;
			end
		3'b011 :
			begin
					ALUSRCA		<= 2'b00;
					ALUSRCB		<= 3'b000;
					ALUOP			<= 2'b00;
					PCWRITE		<= 1'b0;
					PCWRITECOND	<= 1'b0;
					PCSOURCE		<= 2'b00;
					READR1		<= 1'b00;
					case (op)
						6'b111011 : MEM2REG <= 1'b1;	// LWI: MEM2REG = MDR
						default	 : MEM2REG <= 1'b0;	// OTHERWISE: MEM2REG = ALUOUT
					endcase
					IORD			<= 1'b00;
					MEMREAD		<=	1'b00;
					MEMWRITE		<= 1'b00;
					IRWRITE 		<= 1'b00;
					REGWRITE		<= 1'b1;					// ENABLED REGISTER FILE WRITE BACK
			end
		3'b100 :
			begin
					ALUSRCA		<= 2'b01;			// ALUSRC = R1
					ALUSRCB		<= {2'b10,op[0]}; // ALUSRB = 0x0 or 0xFFFFFFFF
					ALUOP			<= {op[0],1'b1};	// ALUOP = ALWAYS OR / ALWAYS AND
					PCWRITE		<= 1'b0;				 
					PCWRITECOND	<= 1'b1;				// CONDITIONAL WRITE IS ENABLED
					PCSOURCE		<= 2'b01;			// SET PC SOURCE TO ALUOUT
					READR1		<= 1'b0;
					MEM2REG		<= 1'b0;
					IORD			<= 1'b0;
					MEMREAD		<=	1'b0;
					MEMWRITE		<= 1'b0;	
					IRWRITE 		<= 1'b0;
					REGWRITE		<= 1'b0;
			end
		3'b101 :
			begin
					ALUSRCA		<= 2'b00;
					ALUSRCB		<= 3'b000;
					ALUOP			<= 2'b00;
					PCWRITE		<= 1'b1;			// PCWRITE ENABLED
					PCWRITECOND	<= 1'b0;
					PCSOURCE		<= 2'b10;		// PCSOURCE = JUMP BITS OF INSTRUCTION
					READR1		<= 1'b0;
					MEM2REG		<= 1'b0;
					IORD			<= 1'b0;
					MEMREAD		<=	1'b0;
					MEMWRITE		<= 1'b0;
					IRWRITE 		<= 1'b0;
					REGWRITE		<= 1'b0;
			end
		3'b110 :
			begin
					case (op)
						6'b111001 : ALUSRCA <= 2'b01;	// LI  : ALUSRCA = A
						6'b111001 : ALUSRCA <= 2'b01; // LUI : ALUSRCA = A
						default   : ALUSRCA <= 2'b10; // All Others : ALUSRCA = 0x0;
					endcase
					ALUSRCB		<= 3'b001;				// ALUSRCB =  TAKE IMMEDIATE
					ALUOP			<= 2'b10;				// ALUOP = TAKE ALU OP COMMAND
					PCWRITE		<= 1'b0;
					PCWRITECOND	<= 1'b0;
					PCSOURCE		<= 2'b00;
					READR1		<= 1'b0;
					MEM2REG		<= 1'b0;
					IORD			<= 1'b0;
					MEMREAD		<=	1'b0;
					MEMWRITE		<= 1'b0;
					IRWRITE 		<= 1'b0;
					REGWRITE		<= 1'b0;
			end
		3'b111 :
			begin
					ALUSRCA		<= 2'b0;
					ALUSRCB		<= 3'b000;
					ALUOP			<= 2'b00;
					PCWRITE		<= 1'b0;
					PCWRITECOND	<= 1'b0;
					PCSOURCE		<= 2'b00;
					READR1		<= 1'b0;
					MEM2REG		<= 1'b0;
					IORD			<= 1'b1;			// IorD = Take ALUOUT as ADDRESS
					MEMREAD		<=	~op[2];		// MEMREAD = 1 or 0 Based on LWI or SWI
					MEMWRITE		<= op[2];		// MEMWRITE = 0 or 1 Based on LWI or SWI
					IRWRITE 		<= 1'b0;
					REGWRITE		<= 1'b0;
			end
		default:
			begin										// Same As State=3'b000
					ALUSRCA		<= 2'b00;
					ALUSRCB		<= 3'b100;
					ALUOP			<= 2'b00;
					PCWRITE		<= 1'b1;
					PCWRITECOND	<= 1'b0;
					PCSOURCE		<= 2'b00;
					READR1		<= 1'b0;
					MEM2REG		<= 1'b0;
					IORD			<= 1'b0;
					MEMREAD		<=	1'b1;
					MEMWRITE		<= 1'b0;
					IRWRITE 		<= 1'b1;
					REGWRITE		<= 1'b0;
			end		
	endcase

end

endmodule

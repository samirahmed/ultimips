`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:01:24 11/14/2011
// Design Name:   alu
// Module Name:   /home/samahmed/Desktop/Project/Milestone4/testALU.v
// Project Name:  datapath
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: alu
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module testALU;

	// Inputs
	reg [31:0] R2;
	reg [31:0] R3;
	reg [4:0] ALUOP;

	// Outputs
	wire [31:0] ALUOUT;
	wire zFlag;

	// Instantiate the Unit Under Test (UUT)
	alu uut (
		.R2(R2), 
		.R3(R3), 
		.ALUOP(ALUOP), 
		.ALUOUT(ALUOUT), 
		.zFlag(zFlag)
	);

	initial begin
		// Initialize Inputs
		R2 = 100; R3 = 0; ALUOP = 0;

		#10; R2 = 1000; 	R3 = 100; 		ALUOP = 4'b0000;	// Test MOV		EXPECT 1000
		#10; R2 = 1000; 	R3 = 100; 		ALUOP = 4'b0001;	// Test NOT		EXPECT 0xFFFFEFFF
		#10; R2 = 1000; 	R3 = -100; 		ALUOP = 4'b0010;	// Test ADD		EXPECT 900
		#10; R2 = -50; 	R3 = 100; 		ALUOP = 4'b0011;	// Test SUB		EXPECT -150
		#10; R2 = 32'h55; R3 = 32'hAA;	ALUOP = 4'b0100;	// Test OR		EXPECT 0x000000FF
		
		#40;	// Break
		
		#10; R2 = 32'h56; R3 = 32'hAA; 	ALUOP = 4'b0101;	// Test AND		EXPECT 0x00000002
		#10; R2 = 32'h55; R3 = 32'hAA; 	ALUOP = 4'b0110;	// Test XOR		EXPECT 0x000000FF
		#10; R2 = 15; 		R3 = -1;			ALUOP = 4'b0111;	// Test SLT		EXPECT 0x00000000
		#10; R2 = -1;	 	R3 = 16;			ALUOP = 4'b0111;	// Test SLT		EXPECT 0x00000001
		
		#40;	// Break
		
		#10; R2 = 32'h12345678; 	R3 = 32'hFFFF7777;	ALUOP = 4'b1001;	// Test LI		EXPECT 0x12347777
		#10; R2 = 32'h12345678; 	R3 = 32'hFFFF7777;	ALUOP = 4'b1010;	// Test LUI		EXPECT 0x77775678
		#10; R2 = 32'h12345678; 	R3 = 32'hFFFF7777;	ALUOP = 4'b1011;	// Test LI		EXPECT 0xFFFF7777
		#10; R2 = 32'h12345678; 	R3 = 32'hFFFF7777;	ALUOP = 4'b1100;	// Test LI		EXPECT 0xFFFF7777
		

		#40; $finish;
        
	end
      
endmodule


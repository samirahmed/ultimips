`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:46:22 11/15/2011
// Design Name:   ImmShift
// Module Name:   /home/samahmed/Desktop/Project/Milestone4/ImmShift_test.v
// Project Name:  datapath
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ImmShift
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ImmShift_test;

	// Inputs
	reg [31:0] IN;

	// Outputs
	wire [31:0] OUT;

	// Instantiate the Unit Under Test (UUT)
	ImmShift uut (
		.IN(IN), 
		.OUT(OUT)
	);

	initial begin
		// Initialize Inputs
		IN = 0;

		// Wait 100 ns for global reset to finish
		#100; IN =  32'h1234FFFF;
      #100; IN =  32'h55555555;
		  
		// Add stimulus here

	end
      
endmodule


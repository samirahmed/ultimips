`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:22:48 11/15/2011 
// Design Name: 
// Module Name:    sandbox 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sandbox( clk,val,REG,OUT
    );

// This is just an testing file for trying different things.
// Sandbox Verilog Testing Mode.


input clk;
input [3:0] val;
output reg 	[3:0]	REG;
output wire [3:0]	OUT;

always @ (posedge clk) begin

	REG <= val;
	
end

assign OUT = REG;

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 	Boston University
// Engineer: 	Samir Ahmed			
// 
// Create Date:    18:59:10 11/14/2011 
// Design Name: 
// Module Name:    alu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description:  ALU for X MIPS CPU designed by samir ahmed
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module alu(
    input [31:0] R2,
    input [31:0] R3,
    input [3:0] ALUOP,
    output [31:0] ALUOUT,
	 output zFlag
    );

assign ALUOUT 	= Result(R2,R3,ALUOP); 
assign zFlag	= isZero(ALUOUT);

// FUNCTION FOR EXECUTING THE ALU
function [31:0] Result;

	input signed[31:0] R2;
	input signed[31:0] R3;
	input [3:0] ALUOP;
	 
	case (ALUOP)
		4'b0000:	Result = R2;							// MOV
		4'b0001:	Result = ~R2;							// NOT
		4'b0010:	Result = R2+R3;						// ADD
		4'b0011:	Result = R2-R3;						// SUB
		4'b0100:	Result = R2|R3;						// OR
		4'b0101:	Result = R2&R3;						// AND
		4'b0110:	Result = R2^R3;						// XOR
		4'b0111:	Result = R2<R3 ? 1:0;				// SLT : If we R2 < R3 give a one else zero
		4'b1001:	Result = {R2[31:16],R3[15:0]};	// Load immediate into bottom 16
		4'b1010: Result = {R3[15:0],R2[15:0]};	// Load immediate into top 16
		4'b1011: Result = R3;							// Return R3
		4'b1100: Result = R3;							// Return R3
		default: Result = R2;
	endcase
endfunction

// FUNCTIO FOR CHECKING IF ZERo
function isZero;
	input [31:0] value;
	isZero = value==0;
endfunction

endmodule

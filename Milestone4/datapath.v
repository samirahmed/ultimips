		`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:52:43 11/15/2011 
// Design Name: 
// Module Name:    datapath 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module datapath(
    input [0:0]	CLK,
	 input [0:0]	RESET,
	 input [1:0]	ALUSRCA,
	 input [2:0]	ALUSRCB,
	 input [1:0]	ALUOP,
	 input [0:0]	PCWRITECOND,
	 input [0:0]	PCSOURCE,
	 input [0:0]	PCWRITE,
	 input [0:0]	READR1,
	 input [0:0]	MEM2REG,
	 input [0:0]	IorD,
	 input [0:0]	MEMREAD,
	 input [0:0]	MEMWRITE,
	 input [0:0]	IRWRITE,
	 input [0:0]	REGWRITE,
	 output[5:0]	OPCODE
    );

	// _____________________________________________________________________________
	// REGISTERS ------------------------------------------------------------------
	reg 	[31:0]	WDR;		// WRITE DATA REGISTER	: WDR
	reg 	[31:0]	MDR;		// MEMORY DATA REGISTER	: MDR
	reg 	[31:0]	PC;		// PROGRAM COUNTER		: PC
	reg 	[31:0]	IR;		// INSTRUCTOR REGISTER	: IR
	reg 	[31:0]	ALUOUT;	// ALUOUTREGISTER			: ALUOUT
	reg 	[31:0]	A;			// REGISTER FILE READ A : A
	reg 	[31:0]	B;			// REGISTER FILE READ B : B
	
	// _____________________________________________________________________________
	// WIRES ----------------------------------------------------------------------
	
	// Memory Register In Wires
	wire	[31:0]	IMEM_out;		// Instruction Memory Out
	wire	[31:0]	DMEM_out;		// Data Memory Out
	
	//	Instruction Memory Register In Wires
	wire	[25:0]	JumpAddr;	// Jump bits
	wire	[15:0]	Imm;				// Immediate
	wire	[4:0]		R1;				// Register 1
	wire	[4:0]		R2;				// Register 2
	wire	[4:0]		R3;				// Register 3
	
	// Register Out Wires
	wire	[31:0]	ALUOUT_out;		// ALUOUT output
	wire	[31:0]	MDR_out;			// MDR output
	wire	[31:0]	WDR_out;			// WDR output
	wire	[31:0]	B_out;			// B out into MUX
	wire	[31:0]	A_out;			// A out into MUX
	
	// Register File Wires
	wire	[31:0]	RF_A;				// WIRE FROM REGISTER FILE TO REGISTER A
	wire	[31:0]	RF_B;				//	WIRE FROM REGISTER FILE TO REGISTER B
	wire	[31:0]	MEM2REG_out;	// WIRE FROM MEM2REG MUX
//	wire	[31:0]	READSELECT1;	//	READ SELECT from READR! MUX
//	wire	[31:0]	READSELECT2;	// READ SELECT from R3
//	wire	[31:0]	WRITESELECT;	// WRITE SELECT from R1
	wire	[4:0]		READR1_out;		// READR1 MUX OUT
	wire	[31:0]	ALUSRCA_out;	// ALUSRCA_out;
	wire	[31:0]	ALUSRCB_out;	// ALUSRCB_out;
	
	// ALU Wires
	wire	[0:0]		zFLAG;			// A Flag that goes off if their is a zero from the ALU
	wire	[31:0]	ALURESULT;		//	RESULT OF ALU
	wire	[3:0]		ALUOP_out;		// Output of the ALUOP 	
	
	// SIGN Ex. Imm Shift
	wire	[31:0]	ExtendedImm;	// Extended Immediate
	
	// PC Wires
	wire	[0:0]		PC_Enable;		// Enables the PC with WRITE and WRITE COND
	wire	[31:0]	PC_out;			// PC Output
	
	// PC SOURCE WIRES
	wire	[31:0]	PCSOURCE_out;	// Wire from PC SOURCE MUX
	wire	[31:0]	JUMP_out;		// Completed Jump Address 
	// _____________________________________________________________________________
	// SubModules ------------------------------------------------------------------
	
	// INSTRUCTION MEMORY
	IMem #(32) IMEM(PC_out,IMEM_out);
	
	// DATA MEMORY
	DMem #(32) DMEM(WDR_out,DMEM_out, ALUOUT_out, MEMWRITE, CLK);
	
	// SIGN EXTENDED 
	signExtender SE(OPCODE[3:0],Imm,ExtendedImm);
	
	// REG FILE 
	nbit_register_file #(5,32) regfile(MEM2REG_out,RF_A,RF_B,READR1_out,R3,R1,REGWRITE,RESET,CLK);

	// ALU 
	alu	ALU(ALUSRCA_out, ALUSRCB_out, ALUOP_out, ALURESULT, zFLAG);				

	// _____________________________________________________________________________
	// Register Drivers ------------------------------------------------------------
	
	// Temp registers for holding current value
	reg [31:0]	currentIR;
	reg [31:0]	currentPC;
	reg [31:0]	currentMDR;

	// Intermediate Registers	
	reg [31:0] newIR;
	reg [31:0] newPC;
	reg [31:0] newMDR;
	
	always @ (posedge CLK) begin
	
	if(RESET) begin
			newIR	<= 32'b0;
			newPC	<= 32'b0;
			newMDR<= 32'b0;
	end
	else begin
		newIR	<= IMEM_out;
		newPC	<= PCSOURCE_out;
		newMDR<= DMEM_out;
	end
	end
	
	
	// ALU RELATED
	always @(posedge CLK) begin	
		
		if (RESET) begin
			// Reset all Values to Zero
			A 			<= 32'b0;
			IR 		<= 32'b0; 
			B 			<= 32'b0;		
			ALUOUT 	<= 32'b0;		
			WDR		<=	32'b0;		
			MDR		<=	32'b0;
			PC 		<= 32'b0;

			// Temps
			currentIR <= 32'b0;
			currentPC <= 32'b0;
			currentMDR<= 32'b0;
			
		end
		else begin
			
			// Registers with no Enable
			A 			<= RF_A;				// Drive Register File into A
			B 			<= RF_B;				// Drive Register File into B
			ALUOUT 	<= ALURESULT;		// Drive to ALUOUT
			WDR		<=	RF_A[15:0];		// Drive into WDR

			// Write Enabled Registers
			case(MEMWRITE)
				1'b1:	MDR 		= DMEM_out;		// Drive new value into MDR
				1'b0: MDR 		= currentMDR;		// Keep the same value
				default:	MDR 	= MDR;			// Keep same value MDR
			endcase
			
			case (IRWRITE) 
				1'b1: 	IR <= IMEM_out; 	// If enabled, drive new value
				1'b0:		IR	<= IR;	// If not enabled, keep old value
				default:	IR <= IR;				// If not enabled, keep old value
			endcase
			
			case (PC_Enable) 
				1'b1:		PC <= PCSOURCE_out; 
				1'b0:		PC <= PC;
				default: PC <= PC;
			endcase
			
			// Temp
			currentIR <= IR;
			currentPC <= PC;
			currentMDR<= MDR;
			
		end
	end

	// _____________________________________________________________________________
	// Combinational Assignments ---------------------------------------------------

	// Jump Address Logic
	assign	JUMP_out		=	 { PC_out[31:26],JumpAddr};

	// PC Enable Logic
	assign	PC_Enable	=	PCWRITE|(PCWRITECOND&zFLAG);
	
	// PC OUT
	assign	PC_out		= PC;
	
	// Register Write Data Logic
	assign	MEM2REG_out	=	MEM2REG ? MDR_out : ALUOUT_out;

	// Register output assignments
	assign	A_out			=	B;
	assign	B_out			=	A;
	assign	ALUOUT_out	=	ALUOUT;
	assign	MDR_out		=	MDR;
	assign	READR1_out	=	READR1 ? R1:R2; 
	assign	WDR_out		= 	WDR;
	
	// Instruction Register Assignments
	assign	Imm	 	= IR[15:0];				// Immediate
	assign	R1	 		= IR[25:21];			// Register 1
	assign	R2			= IR[21:16];			// Register 2
	assign	R3			= IR[15:11];			// Register 3
	assign	JumpAddr = IR[25:0];				// Jump Address
	assign	OPCODE	= IR[31:26];			// OPCODE
	
	
	// _____________________________________________________________________________
	// Large MUX Assignments ---------------------------------------------------
	
	// ALUSRC A
	assign	ALUSRCA_out = ALUSRCA_MUX(ALUSRCA,PC_out,A_out);

	function [31:0] ALUSRCA_MUX;
	input	[1:0]		ALUSRCA;
	input	[31:0]	PC_out;
	input	[31:0]	A_out;
		case(ALUSRCA)
			2'b00 :	ALUSRCA_MUX = PC_out;
			2'b01 :	ALUSRCA_MUX = A_out;
			default:	ALUSRCA_MUX = 32'b0;
		endcase
	endfunction

	// ALUSRC B
	assign	ALUSRCB_out = ALUSRCB_MUX(ALUSRCB,B_out,ExtendedImm);
	
	function [31:0] ALUSRCB_MUX;
		input	[2:0]		ALUSRCB;
		input	[31:0]	B_out;
		input	[31:0]	ExtendedImm;

		case (ALUSRCB) 
			3'b000 : ALUSRCB_MUX = B_out;
			3'b001 : ALUSRCB_MUX = ExtendedImm;
			3'b010 : ALUSRCB_MUX = ExtendedImm;
			3'b011 : ALUSRCB_MUX = 32'b01;
			3'b100 : ALUSRCB_MUX = 32'b00;
			3'b101 : ALUSRCB_MUX = 32'hFFFFFFFF;
			default: ALUSRCB_MUX = B_out;
		endcase
	
	endfunction

	// ALUOP
	assign	ALUOP_out	= ALUOP_MUX( ALUOP, OPCODE[3:0]);
	
	function [3:0] ALUOP_MUX;
		input 	[1:0]	ALUOP; 
		input		[3:0]	OP;
		
		case (ALUOP)
			2'b00:	ALUOP_MUX = 4'b0010;	//	ALWAYS ADD
			2'b01:	ALUOP_MUX =	4'b0100;	// ALWAYS OR
			2'b10:	ALUOP_MUX = OP;		// PASS ON THE OP CODE
			2'b11:	ALUOP_MUX = 4'b0101;	// ALWAYS AND
			default:	ALUOP_MUX = 4'b0010;	// DEFAULT IS ALWAYS ADD
		endcase
	endfunction
	
	// PCSOURCE
	assign	PCSOURCE_out	= PCSOURCE_MUX ( PCSOURCE, JUMP_out, ALURESULT, ALUOUT_out);

	function [31:0]	PCSOURCE_MUX;
	input		[1:0]		PCSOURCE;
	input		[31:0]	JUMP_out;
	input		[31:0]	ALURESULT;
	input		[31:0]	ALUOUT_out;
	
	case (PCSOURCE)
		2'b00	:	PCSOURCE_MUX = ALURESULT;
		2'b01 :	PCSOURCE_MUX = ALUOUT_out;
		2'b10 :	PCSOURCE_MUX = JUMP_out;
		default:	PCSOURCE_MUX = JUMP_out;
	endcase
	endfunction

endmodule

`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:58:37 11/15/2011
// Design Name:   signExtender
// Module Name:   /home/samahmed/Desktop/Project/Milestone4/signextender_test.v
// Project Name:  datapath
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: signExtender
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module signextender_test;

	// Inputs
	reg [3:0] ALUOP;
	reg [15:0] Immediate;

	// Outputs
	wire [31:0] OUT;

	// Instantiate the Unit Under Test (UUT)
	signExtender uut (
		.ALUOP(ALUOP), 
		.Immediate(Immediate), 
		.OUT(OUT)
	);

	initial begin
		// Initialize Inputs
		ALUOP = 0;
		Immediate = 0;

		// Test a Sign Extend Case with 1's then zeros
		#10;	ALUOP = 4'b0000; Immediate = 16'hff77;
		#10;	ALUOP = 4'b0000; Immediate = 16'h77ff;
		
		// Test a Zero Extend Case, expect all 0's
		#10;	ALUOP = 4'b1001; Immediate = 16'hff77;
		#10;	ALUOP = 4'b1001; Immediate = 16'h77ff;

		#10; $finish;
		

	end
      
endmodule


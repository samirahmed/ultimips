`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:27:53 11/15/2011 
// Design Name: 
// Module Name:    signExtender 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module signExtender(
    input [3:0] ALUOP,
    input [15:0] Immediate,
    output [31:0] OUT
    );

assign OUT = Extend(Immediate,ALUOP);

// Function for sign extending or zero extending
function [31:0] 	Extend;

	input [15:0]	Imm;
	input [3:0]		OP;

	case(OP)
		4'b0100:	Extend[31:0] = {16'b0,Imm};
		4'b0101:	Extend[31:0] = {16'b0,Imm};
		4'b0110:	Extend[31:0] = {16'b0,Imm};
		4'b1001:	Extend[31:0] = {16'b0,Imm};
		4'b1010:	Extend[31:0] = {16'b0,Imm};
		4'b1011:	Extend[31:0] = {16'b0,Imm};
		4'b1100:	Extend[31:0] = {16'b0,Imm};
		default:	Extend[31:0] = {{16{Imm[15]}},Imm};
	endcase
endfunction

endmodule

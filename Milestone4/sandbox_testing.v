`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:25:11 11/15/2011
// Design Name:   sandbox
// Module Name:   /home/samahmed/Desktop/Project/Milestone4/sandbox_testing.v
// Project Name:  datapath
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: sandbox
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module sandbox_testing;

	// Inputs
	reg clk;
	reg [3:0] val;
	
	//Outputs
	wire [3:0] OUT;
	wire [3:0] REG;

	// Instantiate the Unit Under Test (UUT)
	sandbox uut (
		.clk(clk), 
		.val(val),
		.OUT(OUT),
		.REG(REG)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		val = 0;

		// Wait 100 ns for global reset to finish
		#10;	val = 7;
		#10;	val = 8;
		#10;	val = 9;
		#20; $finish;
        
		// Add stimulus here

	end
	
	always begin
	#5;	clk = ~clk;
	end
      
endmodule


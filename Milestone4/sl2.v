`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:31:20 11/15/2011 
// Design Name: 
// Module Name:    sl2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sl2( IN,OUT );

parameter SIZE = 26; 
input		[SIZE-1:0] 	IN;
output	[SIZE+1:0]	OUT;

assign OUT = {IN,2'b0};

endmodule

`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   22:08:58 11/17/2011
// Design Name:   datapath
// Module Name:   /home/samahmed/Desktop/Project/Milestone4/datapath_test.v
// Project Name:  datapath
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: datapath
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module datapath_test;

	// Inputs
	reg [0:0] CLK;
	reg [0:0] RESET;
	reg [1:0] ALUSRCA;
	reg [2:0] ALUSRCB;
	reg [1:0] ALUOP;
	reg [0:0] PCWRITECOND;
	reg [0:0] PCSOURCE;
	reg [0:0] PCWRITE;
	reg [0:0] READR1;
	reg [0:0] MEM2REG;
	reg [0:0] IorD;
	reg [0:0] MEMREAD;
	reg [0:0] MEMWRITE;
	reg [0:0] IRWRITE;
	reg [0:0] REGWRITE;

	// Outputs
	wire [5:0] OPCODE;

	// Instantiate the Unit Under Test (UUT)
	datapath uut (
		.CLK(CLK), 
		.RESET(RESET), 
		.ALUSRCA(ALUSRCA), 
		.ALUSRCB(ALUSRCB), 
		.ALUOP(ALUOP), 
		.PCWRITECOND(PCWRITECOND), 
		.PCSOURCE(PCSOURCE), 
		.PCWRITE(PCWRITE), 
		.READR1(READR1), 
		.MEM2REG(MEM2REG), 
		.IorD(IorD), 
		.MEMREAD(MEMREAD), 
		.MEMWRITE(MEMWRITE), 
		.IRWRITE(IRWRITE), 
		.REGWRITE(REGWRITE), 
		.OPCODE(OPCODE)
	);

	initial begin
		// Initialize Inputs
		CLK = 1;
		RESET = 1;
		ALUSRCA = 0;
		ALUSRCB = 3'b000;
		ALUOP = 0;
		PCWRITECOND = 0;
		PCSOURCE = 0;
		PCWRITE = 0;
		READR1 = 0;
		MEM2REG = 0;
		IorD = 0;
		MEMREAD = 0;
		MEMWRITE = 0;
		IRWRITE = 0;
		REGWRITE = 0;
		
		// Add stimulus here
		
		// __________________________________________________________________
		// STATE 0
		#100;
		RESET =0;
		ALUSRCA		= 2'b00;		//ALUSRCA = PC
		ALUSRCB		= 3'b011;	//ALUSRCB = 0x1
		ALUOP			= 2'b00;		//ALUOP = ALWAYS ADD
		PCWRITE		= 1'b1;			//PCWRITE = ENABLED
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;		//PCSOURCE = PC+4
		READR1		= 1'b0;
		MEM2REG		= 1'b0;
		IorD			= 1'b0;			//IORD = TAKE INSTRUCTION ADDRESS
		MEMREAD		=	1'b1;			//MEMREAD = ENABLED
		MEMWRITE		= 1'b0;			
		IRWRITE 		= 1'b1;			//IRWRITE = ENABLED
		REGWRITE		= 1'b0;
		
				// __________________________________________________________________
		// STATE 0
		#100;
		
		ALUSRCA		= 2'b00;		//ALUSRCA = PC
		ALUSRCB		= 3'b011;	//ALUSRCB = 0x1
		ALUOP			= 2'b00;		//ALUOP = ALWAYS ADD
		PCWRITE		= 1'b1;			//PCWRITE = ENABLED
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;		//PCSOURCE = PC+4
		READR1		= 1'b0;
		MEM2REG		= 1'b0;
		IorD			= 1'b0;			//IORD = TAKE INSTRUCTION ADDRESS
		MEMREAD		=	1'b1;			//MEMREAD = ENABLED
		MEMWRITE		= 1'b0;			
		IRWRITE 		= 1'b1;			//IRWRITE = ENABLED
		REGWRITE		= 1'b0;
		
		
		// __________________________________________________________________
		// STATE 1
		#100;

		ALUSRCA		= 2'b00;		//ALUSRCA = PC
		ALUSRCB		= 3'b010;	//ALUSRCB = SE Immediate
		ALUOP			= 2'b00;		//ALUOP 	= ALWAYS ADD
		PCWRITE		= 1'b0;
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;
		case (OPCODE)
			6'b111100 : READR1 = 1'b1;	// SWI: 	READR1 = ENABLED
			6'b111010 : READR1 = 1'b1;	// LUI:	READR1 = ENABLED
			6'b111001 : READR1 = 1'b1;	// LI:	READR1 = ENABLED
			6'b100000 : READR1 = 1'b1;	// BEQZ: READR1 = ENABLED
			6'b100001 : READR1 = 1'b1;	// BNEZ: READR1 = ENABLED
			default   : READR1 = 1'b0;	// ALL OTHER CASES: READR1 = DISABLED
		endcase					
		MEM2REG		= 1'b0;
		IorD			= 1'b0;
		MEMREAD		= 1'b0;
		MEMWRITE		= 1'b0;
		IRWRITE 		= 1'b0;
		REGWRITE		= 1'b0;

		//_______________________________________________________________
		// STATE 6	[EX]
		#100;
		
		case (OPCODE)
			6'b111001 : ALUSRCA = 2'b01;	// LI  : ALUSRCA = A
			6'b111001 : ALUSRCA = 2'b01; // LUI : ALUSRCA = A
			default   : ALUSRCA = 2'b10; // All Others : ALUSRCA = 0x0;
		endcase
		ALUSRCB		= 3'b001;				// ALUSRCB =  TAKE IMMEDIATE
		ALUOP			= 2'b10;				// ALUOP = TAKE ALU OP COMMAND
		PCWRITE		= 1'b0;
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;
		READR1		= 1'b0;
		MEM2REG		= 1'b0;
		IorD			= 1'b0;
		MEMREAD		=	1'b0;
		MEMWRITE		= 1'b0;
		IRWRITE 		= 1'b0;
		REGWRITE		= 1'b0;

		//_______________________________________________________________
		// STATE 3 [WB]
		#100;
		
		ALUSRCA		= 2'b00;
		ALUSRCB		= 3'b000;
		ALUOP			= 2'b00;
		PCWRITE		= 1'b0;
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;
		READR1		= 1'b00;
		case (OPCODE)
			6'b111011 : MEM2REG = 1'b1;	// LWI: MEM2REG = MDR
			default	 : MEM2REG = 1'b0;	// OTHERWISE: MEM2REG = ALUOUT
		endcase
		IorD			= 1'b00;
		MEMREAD		=	1'b00;
		MEMWRITE		= 1'b00;
		IRWRITE 		= 1'b00;
		REGWRITE		= 1'b1;					// ENABLED REGISTER FILE WRITE BACK

		// __________________________________________________________________
		// STATE 0
		#100;

		ALUSRCA		= 2'b00;		//ALUSRCA = PC
		ALUSRCB		= 3'b011;		//ALUSRCB = SHIFTED IMMEDIATED
		ALUOP			= 2'b00;		//ALUOP = ALWAYS ADD
		PCWRITE		= 1'b1;			//PCWRITE = ENABLED
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;		//PCSOURCE = PC+4
		READR1		= 1'b0;
		MEM2REG		= 1'b0;
		IorD			= 1'b0;			//IORD = TAKE INSTRUCTION ADDRESS
		MEMREAD		=	1'b1;			//MEMREAD = ENABLED
		MEMWRITE		= 1'b0;			
		IRWRITE 		= 1'b1;			//IRWRITE = ENABLED
		REGWRITE		= 1'b0;
		
		// __________________________________________________________________
		// STATE 1
		#100;

		ALUSRCA		= 2'b00;		//ALUSRCA = PC
		ALUSRCB		= 3'b010;	//ALUSRCB = SE Immediate
		ALUOP			= 2'b00;		//ALUOP 	= ALWAYS ADD
		PCWRITE		= 1'b0;
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;
		case (OPCODE)
			6'b111100 : READR1 = 1'b1;	// SWI: 	READR1 = ENABLED
			6'b111010 : READR1 = 1'b1;	// LUI:	READR1 = ENABLED
			6'b111001 : READR1 = 1'b1;	// LI:	READR1 = ENABLED
			6'b100000 : READR1 = 1'b1;	// BEQZ: READR1 = ENABLED
			6'b100001 : READR1 = 1'b1;	// BNEZ: READR1 = ENABLED
			default   : READR1 = 1'b0;	// ALL OTHER CASES: READR1 = DISABLED
		endcase					
		MEM2REG		= 1'b0;
		IorD			= 1'b0;
		MEMREAD		= 1'b0;
		MEMWRITE		= 1'b0;
		IRWRITE 		= 1'b0;
		REGWRITE		= 1'b0;

		//_______________________________________________________________
		// STATE 2	[EX]
		#100;

		ALUSRCA		= 2'b01;				// ALUSRCA	= A
		ALUSRCB		= {2'b00,OPCODE[5]};		//	ALUSRCB	= Immediate or B
		ALUOP			= 2'b10;				// ALUOP		= TAKE OPCODE FUNCTION
		PCWRITE		= 1'b0;
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;
		READR1		= 1'b0;
		MEM2REG		= 1'b0;
		IorD			= 1'b0;
		MEMREAD		=	1'b0;
		MEMWRITE		= 1'b0;
		IRWRITE 		= 1'b0;
		REGWRITE		= 1'b0;

		//_______________________________________________________________
		// STATE 3 [WB]
		#100;
		
		ALUSRCA		= 2'b00;
		ALUSRCB		= 3'b000;
		ALUOP			= 2'b00;
		PCWRITE		= 1'b0;
		PCWRITECOND	= 1'b0;
		PCSOURCE		= 2'b00;
		READR1		= 1'b00;
		case (OPCODE)
			6'b111011 : MEM2REG = 1'b1;	// LWI: MEM2REG = MDR
			default	 : MEM2REG = 1'b0;	// OTHERWISE: MEM2REG = ALUOUT
		endcase
		IorD			= 1'b00;
		MEMREAD		=	1'b00;
		MEMWRITE		= 1'b00;
		IRWRITE 		= 1'b00;
		REGWRITE		= 1'b1;					// ENABLED REGISTER FILE WRITE BACK

		#100; $finish;
		
		
		//
		
	end
      
		always begin
		#50;	CLK = ~CLK;
		end
		
endmodule


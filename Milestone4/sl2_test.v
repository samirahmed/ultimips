`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:39:06 11/15/2011
// Design Name:   sl2
// Module Name:   /home/samahmed/Desktop/Project/Milestone4/sl2_test.v
// Project Name:  datapath
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: sl2
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module sl2_test;

	// Inputs
	reg [25:0] IN;

	// Outputs
	wire [27:0] OUT;

	// Instantiate the Unit Under Test (UUT)
	sl2 uut (
		.IN(IN), 
		.OUT(OUT)
	);

	initial begin
		// Initialize Inputs
		IN = 0;

		// Wait 100 ns for global reset to finish
		#100; IN = 26'b_0000_1111_1111_0000_1111_1111_00;
		#100; IN = 26'b_0000_1111_1111_0000_1111_1111_11;        
		// Add stimulus here

	end
      
endmodule


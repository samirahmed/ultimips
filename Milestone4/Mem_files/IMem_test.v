`timescale 1ns / 1ps

module IMem_test;

parameter DATA_WIDTH = 32;

//-------------Input Ports-----------------------------
reg [DATA_WIDTH-1:0] PC;
//-------------Output Ports----------------------------
wire [DATA_WIDTH-1:0] Instruction;
//-------------Wires-----------------------------------
//-------------Other-----------------------------------
integer i;
//------------Code Starts Here-------------------------

	// Instantiate the Unit Under Test (UUT)
	IMem uut (
      .PC(PC), 
		.Instruction(Instruction)
	);
 
	// Test vectors
	initial begin

		PC= 0;

		// Loop over all instructions in program
		for(i= 0;i < uut.PROG_LENGTH;i= i + 1)
		begin
			#10;
			if(PC >= uut.PROG_LENGTH-1) PC= 0;
			else PC= PC + 1;
		end
		#10;

	end
      
endmodule


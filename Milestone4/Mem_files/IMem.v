// IMem
//
// A module used to mimic Instruction memory, for the EC413 project.
// Returns hardcoded instructions based on the current PC.
//
// DATA_WIDTH: instruction and data width (i.e 32 bits).
//
// 11/12: Changed into pure combinatoric module.
// 11/16: New programs: PROGRAM_2, PROGRAM_3.
//
`timescale 1ns / 1ps
// ----------------------------------------------------
// IMPORTANT!
// Which test program to use:
// - PROGRAM_1: first simple hello world example.
//              covers all state pathways but 
//              not all instructions.
// - PROGRAM_2: test remaining instructions 
//              and corner cases.
// - PROGRAM_3: optional LW/SW program.
//
`define PROGRAM_0 // <<<<<<<<<<<< CHANGE TEST PROGRAM HERE!
//
// Change the previous line to try a different program,
// when available.
// ----------------------------------------------------

module IMem(PC,          // PC (address) of instruction in IMem
            Instruction);

parameter DATA_WIDTH= 32;

`ifdef PROGRAM_1
	parameter PROG_LENGTH= 16;
`else
`ifdef PROGRAM_2
	parameter PROG_LENGTH= 32;
`else
`ifdef PROGRAM_3
	parameter PROG_LENGTH= 15;
`endif
`endif
`endif
//-------------Input Ports-----------------------------
input [DATA_WIDTH-1:0] PC;
//-------------Output Ports----------------------------
output [DATA_WIDTH-1:0] Instruction;
reg [DATA_WIDTH-1:0] Instruction;
//------------Code Starts Here-------------------------
always @(PC)
begin
case(PC)
//-----------------------------------------------------
`ifdef PROGRAM_0

	// LI	R3	0xFFFF
	0:	Instruction = 32'b111001_00011_00000_1111111111111111;
	
	// ADD r4 R3 R3
	1:	Instruction = 32'b010010_00100_00011_00011_00000000000;
	
	
`else
//-----------------------------------------------------
`ifdef PROGRAM_1
//-----------------------------------------------------

	//
	// PROGRAM_1: Hello World Test
	//

	//
	// 1.1) First part: 
	// Load values into:
	// $R0 = -1
	// $R1 = 0
	// $R2 = 2
	// Add $R0 and $R2 and get an answer in $R3: 
	// -1 + 2 = 1
	//

	// LI   $R0, 0xFFFF
	0: Instruction=  32'b111001_00000_00000_1111111111111111;
	// LUI  $R0, 0xFFFF
	1: Instruction=  32'b111010_00000_00000_1111111111111111;
	// LI   $R1, 0x0000
	2: Instruction=  32'b111001_00001_00000_0000000000000000;
	// LUI  $R1, 0x0000
	3: Instruction=  32'b111010_00001_00000_0000000000000000;
	// LI   $R2, 0x0002
	4: Instruction=  32'b111001_00010_00000_0000000000000010;
	// LUI  $R2, 0x0000
	5: Instruction=  32'b111010_00010_00000_0000000000000000;
	// ADD  $R3, $R0, $R2
	6: Instruction=  32'b010010_00011_00000_00010_00000000000;

	//
	// 1.2) Second part: store and load, should store $R3
	// (contains 1) into address 5.  Then load from 
	// address 5 into register $R1.  $R1 should then 
	// contain 1.
	//

	// SWI  $R3, [0x5]
	7: Instruction=  32'b111100_00011_00000_0000000000000101;
	// LWI  $R1, [0x5]
	8: Instruction=  32'b111011_00001_00000_0000000000000101;


	//
	// 1.3) Third part: simple loop test, loop $R0 from -1 to 1
	//      $R0 at instruction 11 goes through values: 0, 1.
	//

	// ADDI $R0, $R0, 0x0001
	9: Instruction=  32'b110010_00000_00000_0000000000000001;
	// SLT  $R31, $R0, $R1
	10: Instruction= 32'b010111_11111_00000_00001_00000000000;
	// BNEZ $R31, 0xFFFD
	11: Instruction= 32'b100001_11111_00000_1111111111111101;

	//
	// 1.4) Fourth part: test jump by _skipping_ load instructions
	// at PCs 13 and 14.  Contents of $R0 should still be 1.
	// Afterwards 1 is subtracted with SUBI and final output 
	// should be 0.
	//

	// J    15
	12: Instruction= 32'b000001_00000000000000000000001111;
	// LI   $R0, 0xFFFF
	13: Instruction= 32'b111001_00000_00000_1111111111111111;
	// LUI  $R0, 0xFFFF
	14: Instruction= 32'b111010_00000_00000_1111111111111111;
	// SUBI $R0, $R0, 0x0001
	15: Instruction= 32'b110011_00000_00000_0000000000000001;
`else
//-----------------------------------------------------
`ifdef PROGRAM_2
//-----------------------------------------------------

	//
	// PROGRAM_2:
	// Test all R-type and logical/arithmetic I-type instructions, 
	// also test remaining untested instructions and additional
	// corner cases.
	//

	//
	// 2.1) First part: 
	// Load values into:
	// $R0 = -2    (0xFFFFFFFE)
	// $R1 = 65537 (0x00010001)
	// $R2 = 1     (0x00000001)
	//

	// LI   $R0, 0xFFFE
	0: Instruction=  32'b111001_00000_00000_1111111111111110;
	// LUI  $R0, 0xFFFF
	1: Instruction=  32'b111010_00000_00000_1111111111111111;
	// LI   $R1, 0x0001
	2: Instruction=  32'b111001_00001_00000_0000000000000001;
	// LUI  $R1, 0x0001
	3: Instruction=  32'b111010_00001_00000_0000000000000001;
	// LI   $R2, 0x0001
	4: Instruction=  32'b111001_00010_00000_0000000000000001;
	// LUI  $R2, 0x0000
	5: Instruction=  32'b111010_00010_00000_0000000000000000;
	
	//
	// 2.2) Second part: 
	// Run through all R-Type instructions
	//
	
	// MOV  $R3, $R2: ALUOutput = 1
	6: Instruction=  32'b010000_00011_00010_00000_00000000000;
	// NOT  $R4, $R2: ALUOutput = (~0x00000001) = 0xFFFFFFFE
	7: Instruction=  32'b010001_00100_00010_00000_00000000000;
	// ADD  $R5, $R2, $R0: ALUOutput = 1 + (-2) = -1 (0xFFFFFFFF)
	8: Instruction=  32'b010010_00101_00010_00000_00000000000;
	// SUB  $R6, $R2, $R0: ALUOutput = 1 - (-2) = 3
	9: Instruction=  32'b010011_00110_00010_00000_00000000000;
	// OR   $R7, $R1, $R0: ALUOutput = (0x00010001 | 0xFFFFFFFE) = 0xFFFFFFFF
	10: Instruction= 32'b010100_00111_00001_00000_00000000000;
	// AND  $R8, $R1, $R0: ALUOutput = (0x00010001 & 0xFFFFFFFE) = 0x00010000
	11: Instruction= 32'b010101_01000_00001_00000_00000000000;
	// XOR  $R9, $R1, $R0: ALUOutput = (0x00010001 ^ 0xFFFFFFFE) = 0xFFFEFFFF
	12: Instruction= 32'b010110_01001_00001_00000_00000000000;
	// SLT  $R10, $R1, $R0: ALUOutput = (0x00010001 < -2) = 0
	13: Instruction= 32'b010111_01010_00001_00000_00000000000;
	// SLTU $R11, $R1, $R0: ALUOutput = (0x00010001 < 0xFFFFFFFE) = 1
	14: Instruction= 32'b011000_01011_00001_00000_00000000000;
	
	//
	// 2.3) Third part: 
	// Run through all Logical/Arithmetic I-Type instructions
	//
	
	// ADDI  $R12, $R2, 5: ALUOutput = 1 + 5 = 6
	15: Instruction= 32'b110010_01100_00010_0000000000000101;
	// SUBI  $R13, $R2, 5: ALUOutput = 1 - 5 = -4
	16: Instruction= 32'b110011_01101_00010_0000000000000101;
	// ORI   $R14, $R2, 5: ALUOutput = (0x00000001 | 0x0005) = 0x00000005
	17: Instruction= 32'b110100_01110_00010_0000000000000101;
	// ANDI  $R15, $R2, 5: ALUOutput = (0x00000001 & 0x0005) = 0x00000001
	18: Instruction= 32'b110101_01111_00010_0000000000000101;
	// XORI  $R16, $R2, 5: ALUOutput = (0x00000001 ^ 0x0005) = 0x00000004
	19: Instruction= 32'b110110_10000_00010_0000000000000101;
	// SLTI  $R17, $R2, 5: ALUOutput = (1 < 5) = 1
	20: Instruction= 32'b110111_10001_00010_0000000000000101;
	// SLTUI $R18, $R2, 5: ALUOutput = (1 < 5) = 1
	21: Instruction= 32'b111000_10010_00010_0000000000000101;

	//
	// 2.4) Fourth part: 
	// LWI and SWI, test corner cases
	//
	
	// SWI  $R3, [0x0000] (store 1 at address 0)
	22: Instruction= 32'b111100_00011_00000_0000000000000000;
	// SWI  $R4, [0x0000] (overwrite value of 1 with 0xFFFFFFFE at address 0)
	23: Instruction= 32'b111100_00100_00000_0000000000000000;
	// SWI  $R5, [0xFFFF] (store 0xFFFFFFFF at address 0xFFFF)
	24: Instruction= 32'b111100_00101_00000_1111111111111111;
	// LWI  $R19, [0x0000] (load 0xFFFFFFFE)
	25: Instruction= 32'b111011_10011_00000_0000000000000000;
	// ADDI  $R19, $R19, 1: ALUOutput = 0xFFFFFFFF
	26: Instruction= 32'b110010_10011_10011_0000000000000001;
	// LWI  $R19, [0xFFFF] (load 0xFFFFFFFF)
	27: Instruction= 32'b111011_10011_00000_1111111111111111;
	// ADDI  $R19, $R19, 1: ALUOutput = 0x00000000
	28: Instruction= 32'b110010_10011_10011_0000000000000001;

	//
	// 2.5) Fifth part: 
	// BEQZ test, loops $R0 from -2 to 2
	//      $R0 at instruction 31 goes through values: -1, 0, 1, 2.
	//
	// ADDI $R0, $R0, 0x0001
	29: Instruction= 32'b110010_00000_00000_0000000000000001;
	// SLT  $R31, $R2, $R0
	30: Instruction= 32'b010111_11111_00010_00000_00000000000;
	// BEQZ $R31, 0xFFFD
	31: Instruction= 32'b100000_11111_00000_1111111111111101;
	
`else
//-----------------------------------------------------
`ifdef PROGRAM_3
//-----------------------------------------------------

   // Simple LW and SW test.

	//
	// 3.1) First part: 
	// Load values into:
	// $R0 = 0    (0x00000000)
	// $R1 = 10   (0x0000000A)
	//

	// LI   $R0, 0x0000
	0: Instruction=  32'b111001_00000_00000_0000000000000000;
	// LUI  $R0, 0x0000
	1: Instruction=  32'b111010_00000_00000_0000000000000000;
	// LI   $R1, 0x000A
	2: Instruction=  32'b111001_00001_00000_0000000000001010;
	// LUI  $R1, 0x0000
	3: Instruction=  32'b111010_00001_00000_0000000000000000;

	//
	// 3.2) Second part: 
	// Loop with SW test.
	// Stores the following:
	//
	// Address 1:  0
	// Address 2:  1
	// ...
	// Address 9:  8
	// Address 10: 9
	//
	
	// SW  $R0, $R0[0x1]
	4: Instruction=  32'b111110_00000_00000_0000000000000001;
	// ADDI $R0, $R0, 0x0001
	5: Instruction=  32'b110010_00000_00000_0000000000000001;
	// SLT  $R31, $R0, $R1
	6: Instruction=  32'b010111_11111_00000_00001_00000000000;
	// BNEZ $R31, 0xFFFC
	7: Instruction=  32'b100001_11111_00000_1111111111111100;
	
	//
	// 3.3) Third part: 
	// Loop with LW test.
	// Should output out of the ALU on instruction 11: 1, 2, ..., 10
	//
	
	// LI   $R0, 0x0000
	8: Instruction=  32'b111001_00000_00000_0000000000000000;
	// LUI  $R0, 0x0000
	9: Instruction=  32'b111010_00000_00000_0000000000000000;
	// LW  $R19, $R0[0x0001]
	10: Instruction= 32'b111101_10011_00000_0000000000000001;
	// ADDI $R19, $R19, 0x0001
	11: Instruction= 32'b110010_10011_10011_0000000000000001;
	// ADDI $R0, $R0, 0x0001
	12: Instruction= 32'b110010_00000_00000_0000000000000001;
	// SLT  $R31, $R0, $R1
	13: Instruction= 32'b010111_11111_00000_00001_00000000000;
	// BNEZ $R31, 0xFFFB
	14: Instruction= 32'b100001_11111_00000_1111111111111011;

`endif
`endif
`endif
`endif
	default: Instruction= 0; //NOOP
endcase
end

endmodule
	

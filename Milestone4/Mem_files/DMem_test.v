`timescale 1ns / 1ps

module DMem_test;

parameter ADDRESS_WIDTH = 16;
parameter DATA_WIDTH = 32;

//-------------Input Ports-----------------------------
reg [DATA_WIDTH-1:0]       WriteData;
reg [ADDRESS_WIDTH-1:0]    Address;
reg MemWrite;
reg Clk;
//-------------Output Ports----------------------------
wire [DATA_WIDTH-1:0] MemData;
//-------------Wires-----------------------------------
//-------------Other-----------------------------------
//------------Code Starts Here-------------------------

	// Instantiate the Unit Under Test (UUT)
	DMem #(ADDRESS_WIDTH, DATA_WIDTH) uut (
      .WriteData(WriteData),
      .MemData(MemData),
		.Address(Address),
      .MemWrite(MemWrite),  
      .Clk(Clk)
	);
 
	// Test vectors
	always begin

		
		// Initial Reset, should only occur on rising edge
		     WriteData= 32'h00000000; Address= 0; MemWrite= 0; Clk= 0;
		#20  WriteData= 32'h00000000; Address= 0; MemWrite= 0; Clk= 1;

		#20  WriteData= 32'hFFFFFFFF; Address= 0; MemWrite= 1; Clk= 0;
		#20  WriteData= 32'hFFFFFFFF; Address= 0; MemWrite= 1; Clk= 1;
		
		#20  WriteData= 32'hAAAAAAAA; Address= 1024; MemWrite= 1; Clk= 0;
		#20  WriteData= 32'hAAAAAAAA; Address= 1024; MemWrite= 1; Clk= 1;
		
		#20  WriteData= 32'hAAAAAAAA; Address= 1024; MemWrite= 0; Clk= 0;
		#20  WriteData= 32'hAAAAAAAA; Address= 1024; MemWrite= 0; Clk= 1;
		
		#20  WriteData= 32'h55555555; Address= 0; MemWrite= 0; Clk= 0;
		#20  WriteData= 32'h55555555; Address= 0; MemWrite= 0; Clk= 1;
		
		#20  WriteData= 32'h55555555; Address= 1024; MemWrite= 0; Clk= 0;
		#20  WriteData= 32'h55555555; Address= 1024; MemWrite= 0; Clk= 1;
		
		#20  WriteData= 32'h55555555; Address= 65535; MemWrite= 1; Clk= 0;
		#20  WriteData= 32'h55555555; Address= 65535; MemWrite= 1; Clk= 1;
		
		#20  WriteData= 32'h00000000; Address= 65535; MemWrite= 0; Clk= 0;
		#20  WriteData= 32'h00000000; Address= 65535; MemWrite= 0; Clk= 1;

		#20  $finish;      // Terminate simulation
	end
      
endmodule


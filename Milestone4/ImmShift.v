`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:43:54 11/15/2011 
// Design Name: 
// Module Name:    ImmShift 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ImmShift(
    input [31:0] IN,
    output [31:0] OUT
    );

assign OUT = {IN[29:0],2'b00};

endmodule

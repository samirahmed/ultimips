`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:01:11 10/24/2011
// Design Name:   ten_state_machine
// Module Name:   /home/samahmed/Desktop/Project/Milestone_1/test_state.v
// Project Name:  state_machine
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ten_state_machine
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_state;

	// Inputs
	reg [31:0] in;
	reg clk;
	reg reset;

	// Outputs
	wire [3:0] state;

	// Instantiate the Unit Under Test (UUT)
	ten_state_machine uut (
		.in(in), 
		.state(state), 
		.clk(clk),
		.reset(reset)
	);

	initial begin
		// Initialize Inputs
		in = 0;
		clk = 0;
		reset = 1;
		#100; reset = 0; #100;
		
		// Expect States 0 > 1 > 6 > 7 > 0
		#100; in =32'b000000_00000_00000_00000_00000_000000;
		
		// Expect States 0 > 1 > 9 > 0
		#100; in =32'b000011_00000_00000_00000_00000_000000;
		
		// Expect States 0 > 1 > 8 > 0
		#100; in =32'b000100_00000_00000_00000_00000_000000;
		
		// Expect States 0 > 1 > 6 > 7 > 0
		#100; in =32'b000000_00000_00000_00000_00000_000000;
		
		// Expect States 0 > 1 > 2 > 3 > 4 > 0
		#200; in =32'b100011_00000_00000_00000_00000_000000;
		
		// Expect States 0 > 1 > 2 > 5 > 0
		#200; in =32'b101011_00000_00000_00000_00000_000000;
		
		// Expect States 0 > 1 > 0
		#200; in =32'b101111_00000_00000_00000_00000_000000;
		
		// Expect States 0 > 1 > 2 > 5 > 0
		#200; in =32'b101011_00000_00000_00000_00000_000000;
		
		#200; $finish;
	end
      
		
	always begin
	#5; clk = ~clk; 
	end
	
endmodule


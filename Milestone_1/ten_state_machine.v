`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:20:51 10/24/2011 
// Design Name: 
// Module Name:    ten_state_machine 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ten_state_machine( in , state, clk, reset
    );

// Inputs and outputs and wires
input wire [31:0] in;
input wire clk;
input wire reset;

output reg [3:0] state;
reg [3:0] next_state;

// If reset is true then 
always @( posedge clk)  begin

if (reset) state = 4'b0000;
else state = next_state;
	
end


always @ ( posedge clk) begin

	case (state)
		4'b0000 : next_state = 4'b0001;
		4'b0001 : case ( in[31:26] )
					6'b000000 : next_state = 4'b0110;
					6'b000011 : next_state = 4'b1001;
					6'b000100 : next_state = 4'b1000;
					6'b100011 : next_state = 4'b0010;
					6'b101011 :	next_state = 4'b0010;
					default : 	next_state = 4'b0000;
					endcase
		4'b0010 : begin
						if (in[31:26] == 6'b100011) next_state = 4'b0011;
						if (in[31:26] == 6'b101011) next_state = 4'b0101;
					end
		4'b0011 : next_state = 4'b0100;
		4'b0100 : next_state = 4'b0000;
		4'b0101 : next_state = 4'b0000; 
		4'b0110 : next_state = 4'b0111;
		4'b0111 : next_state = 4'b0000;
		4'b1000 : next_state = 4'b0000;
		4'b1001: next_state = 4'b0000;		
		default: next_state = 4'b0000;
	endcase
	end
	
endmodule
	


